#API Sample
###Summary
---
This is a simple API application based off of RQ and Redis. You can read more on this elsewhere but this is really just a simple structure to be able to apply queues vs having to thread within an application.

###Requirements
---
* Redis Server on Server
* redis
* rq


###Usage
---
1) Launch the ```worker.py``` to run in the background.

2) Then just execute ```sample_application.py```
