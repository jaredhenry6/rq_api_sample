#Sample API Application 
import time

#Redis information for the redis-queue.
from redis import Redis
from rq import Queue
q = Queue(connection=Redis())


def sample_application(sleeping):
    time.sleep(sleeping)
    print ("Hello, I slept for {0}".format(str(sleeping)))
    return "Hello, I slept for {0}".format(str(sleeping))




