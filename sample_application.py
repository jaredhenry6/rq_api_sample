from redis import Redis
import os
from rq import Queue
from api_worker import sample_application
from worker import conn
q = Queue(connection=conn)


for n in range(15):
	result = q.enqueue(sample_application, args=(n,))
	print (result)
